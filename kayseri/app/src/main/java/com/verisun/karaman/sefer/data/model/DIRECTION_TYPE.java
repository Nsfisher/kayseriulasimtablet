package com.verisun.karaman.sefer.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public enum DIRECTION_TYPE implements Parcelable {
    @SerializedName("ARRIVAL")
    ARRIVAL,
    @SerializedName("DEPARTURE")
    DEPARTURE;

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ordinal());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DIRECTION_TYPE> CREATOR = new Creator<DIRECTION_TYPE>() {
        @Override
        public DIRECTION_TYPE createFromParcel(Parcel in) {
            final int ordinal = in.readInt();
            switch (ordinal) {
                case 0:
                    return ARRIVAL;
                default:
                    return DEPARTURE;
            }
        }

        @Override
        public DIRECTION_TYPE[] newArray(int size) {
            return new DIRECTION_TYPE[size];
        }
    };
}
