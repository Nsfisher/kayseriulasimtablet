package com.verisun.karaman.sefer.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 29/08/16.
 */
public class Search implements Parcelable {
    /***
     * {
     "type": "BusLine",
     "node": {
     "code": "3",
     "description": "1 - GAR YERİ HASANPAŞA GARAJI'dır.\r\n2 - PAZAR GÜNLERİ OTO ÇALIŞMAZ.",
     "garage": "Anadolu",
     "journeyDuration": 70,
     "name": "BURHANİYE MAHALLESİ - KADIKÖY",
     "type": "NORMAL",
     "directions": [
     "DEPARTURE",
     "ARRIVAL"
     ],
     "city": {
     "code": "IST",
     "name": "İstanbul",
     "id": "1"
     },
     "iconType": "BUS_ICON",
     "id": "3195"
     }
     }
     */
    @SerializedName("type")
    public String type;

    @SerializedName("node")
    public Node node;

    protected Search(Parcel in) {
        type = in.readString();
        node = in.readParcelable(Node.class.getClassLoader());
    }

    public static final Creator<Search> CREATOR = new Creator<Search>() {
        @Override
        public Search createFromParcel(Parcel in) {
            return new Search(in);
        }

        @Override
        public Search[] newArray(int size) {
            return new Search[size];
        }
    };

    public Node getNode() {
        return node;
    }

    public String getType() {
        return type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeParcelable(node, flags);
    }
}
