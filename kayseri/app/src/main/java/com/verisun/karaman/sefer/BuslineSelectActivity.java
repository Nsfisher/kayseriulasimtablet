package com.verisun.karaman.sefer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.verisun.core.App;
import com.verisun.core.BaseActivity;

import com.verisun.core.Logger;
import com.verisun.core.Service;
import com.verisun.core.Services;
import com.verisun.karaman.sefer.data.model.BusLine;
import com.verisun.karaman.sefer.data.model.Disable;
import com.verisun.karaman.sefer.data.model.DisabledDetail;
import com.verisun.karaman.sefer.data.model.NewBusLine;


import com.verisun.karaman.sefer.data.model.NewBusLineDetail;
import com.verisun.karaman.sefer.data.model.Node;
import com.verisun.karaman.sefer.data.model.Search;
import com.verisun.karaman.sefer.utils.SearchAdapter;
import com.verisun.utils.LocationService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnItemClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class BuslineSelectActivity extends BaseActivity {



    @Bind(R.id.button_ok)
    View buttonOk;
    @Bind(R.id.button_cancel)
    View buttonCancel;
    @Bind(R.id.delete)
    View buttonDelete;
    @Bind(R.id.busline_searchview)
    EditText buslineEditText;
    @Bind(android.R.id.list)
    ListView buslineList;

    // BuslineAdapter adapter;
    private Service service;
    private Services services;
    private Service disableService;
    private TextView checkData;
    private ArrayList<Search> busLineArrayList;
    private ArrayList<Node> newBusLineArrayList;

    private NewSearchAdapter searchAdapter;
    static String startBus = null;
      static  String stopBus = null;


    private boolean firstStart;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_select);
        setResult(RESULT_CANCELED);


        checkData=(TextView)findViewById(R.id.check_data);
        firstStart = getIntent().getBooleanExtra(MainActivity.EXTRA_FIRST_START, false);
        if (firstStart) {
            buttonCancel.setVisibility(View.INVISIBLE);
        }
        buttonOk.setVisibility(View.INVISIBLE);
        searchAdapter = new NewSearchAdapter();
        // adapter = new BuslineAdapter(this,FAKE_DATA);
        buslineList.setAdapter(searchAdapter);




        ((EditText) findViewById(R.id.busline_searchview)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                char[] chars = new char[s.length()];

                s.getChars(0, s.length(), chars, 0);

                //search(new String(chars));
                searchNew(new String(chars));


            }

            private String trim(Editable s) {
                return s.toString().trim();
            }
        });


        final Observable<CharSequence> observable = RxTextView.textChanges(buslineEditText)
                .debounce(100, TimeUnit.MILLISECONDS);

        //Change button state depending on the text is empty or not.
        observable.map(text -> !TextUtils.isEmpty(text))
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(buttonDelete::setEnabled);

        observable.subscribe(text -> resetAdapter(text.toString()));

        //disableSoftInputMethod
        // buslineEditText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        // buslineEditText.setTextIsSelectable(true);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

//    private void search(String keyword) {
//        String test = null;
//
//        service = Service.service;
//        try {
//            test = URLDecoder.decode(URLEncoder.encode(keyword, "iso8859-1"), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        service.search(keyword, new Service.BaseCallBack<ArrayList<NewBusLine>>() {
//            @Override
//            public void onSuccess(ArrayList<NewBusLine> newBusLines) {
//                busLineArrayList = new ArrayList<>();
//                newBusLineArrayList = new ArrayList<>();
//                busLineArrayList = newBusLines;
//                searchAdapter.notifyDataSetChanged();
//                parseeBusline();
//
//                Log.e("asd", "asd");
//            }
//
//            @Override
//            public void onError(String errorMessage) {
//
//            }
//
//        });
//
//    }

    private void searchNew(String keyword) {
        services = Services.service;
        services.search(App.city_ID, keyword, new Services.BaseCallBack<ArrayList<Search>>() {
            @Override
            public void onSuccessResponse(ArrayList<Search> searchables) {
                busLineArrayList = new ArrayList<>();
                newBusLineArrayList = new ArrayList<>();
                busLineArrayList = searchables;
                searchAdapter.notifyDataSetChanged();
                parseBusline();
                Log.e("","");
            }
            @Override
            public void onError(String errorMessage) {
                Log.e("","");
            }
        });
    }

    private void parseBusline() {
        newBusLineArrayList.clear();

        for (int i = 0; i < busLineArrayList.size(); i++) {
            if (busLineArrayList.get(i).getType().equals("BusLineDirection")) {
                newBusLineArrayList.add(busLineArrayList.get(i).getNode());
            }

        }
        searchAdapter.setListObjects(newBusLineArrayList);
        if (newBusLineArrayList.size() == 0) {
            checkData.setVisibility(View.VISIBLE);
        } else {
            checkData.setVisibility(View.GONE);
        }

    }

    private void resetAdapter(final String text) {
        //TODO reset the adapter with network request.
    }

    @Override
    protected String getAnalyticsScreenName() {
        return "Hat numarası seçim";
    }

    @Override
    public void onBackPressed() {
        if (!firstStart) {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.button_cancel)
    public void onClickCancel() {
        finish();
    }

    @OnClick({R.id.number_0,
            R.id.number_1,
            R.id.number_2,
            R.id.number_3,
            R.id.number_4,
            R.id.number_5,
            R.id.number_6,
            R.id.number_7,
            R.id.number_8,
            R.id.number_9})
    public void onNumberPadClick(Button numberView) {
        buslineEditText.append(numberView.getText());
    }

    @OnClick(R.id.delete)
    public void onDeleteClick() {
        CharSequence text = buslineEditText.getText();
        int selectionEnd = buslineEditText.getSelectionEnd();
        if (!TextUtils.isEmpty(text) && selectionEnd > 0) {
            SpannableStringBuilder selectedStr = new SpannableStringBuilder(text);
            selectedStr = selectedStr.replace(selectionEnd - 1, selectionEnd, "");
            buslineEditText.setText(selectedStr);
            buslineEditText.setSelection(selectionEnd - 1);
        }
    }

    @OnItemClick(android.R.id.list)
    public void onListItemClick(int position) {
        final Node item = (Node) searchAdapter.getItem(position);

        Intent data = new Intent(this,MainActivity.class);
        data.putExtra("busline",item);
        data.putExtra("start",startBus);
        data.putExtra("stop",stopBus);
        setResult(RESULT_OK, data);
        finish();


    }

    public ArrayList<Disable> checkDisabled(String busID) {
        disableService = Service.disabledService;
        disableService.checkDisabled(busID, new Service.BaseCallBack<ArrayList<Disable>>() {
            @Override
            public void onSuccess(ArrayList<Disable> disables) {

            }

            @Override
            public void onError(String errorMessage) {

            }
        });
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "BuslineSelect Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.verisun.karaman.sefer/http/host/path")
        );
        //AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "BuslineSelect Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.verisun.karaman.sefer/http/host/path")
        );
       // AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /*
        private static class BuslineAdapter extends ArrayAdapter<BusLine> {

            public BuslineAdapter(Context context, BusLine[] objects) {
                super(context, R.layout.list_item_busline, objects);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);

                final BusLine item = getItem(position);

                SpannableStringBuilder text = new SpannableStringBuilder();
                appendColoredText(text, item.getCode())
                        .append(" ")
                        .append(item.getName());
                v.setText(text);
                return v;
            }

            @SuppressWarnings("deprecation")
            private SpannableStringBuilder appendColoredText(SpannableStringBuilder text, String code) {
                final int primaryColor = getContext().getResources().getColor(R.color.colorPrimary);
                final int start = text.length();
                text.append(code);
                text.setSpan(new ForegroundColorSpan(primaryColor), start, text.length(), 0);

                return text;
            }
        }

      */
    private class NewSearchAdapter extends SearchAdapter<ArrayList<NewBusLineDetail>> {

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Viewholder viewholder;
            Node getnewBusLine = (Node) getItem(position);
            if (convertView == null) {
                viewholder = new Viewholder();
                convertView = getLayoutInflater().inflate(R.layout.list_item_busline, parent, false);
                viewholder.name = (TextView) convertView.findViewById(R.id.name);
                viewholder.busLineCode = (TextView) convertView.findViewById(R.id.code);
                viewholder.direction = (TextView) convertView.findViewById(R.id.txt_item_direction);
                convertView.setTag(viewholder);

            } else {
                viewholder = (Viewholder) convertView.getTag();
            }
            if (getnewBusLine != null) {
                viewholder.name.setText(getnewBusLine.getBusLineDetail().getName());
                viewholder.busLineCode.setText(getnewBusLine.getBusLineDetail().getCode());
                viewholder.direction.setText(getResources().getString(R.string.direction)+" "+getnewBusLine.getDirectionDescription());
            }


            return convertView;
        }

        class Viewholder {
            TextView name;
            TextView busLineCode;
            TextView direction;
        }
    }



}
