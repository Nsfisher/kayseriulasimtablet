package com.verisun.karaman.sefer.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 5/31/17.
 */

public class ReportBody {

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("busNo")
    private String busNo;

    @SerializedName("busLine")
    private ReportBusline busline;

    @SerializedName("type")
    private String type;


    public void setType(String type) {
        this.type = type;
    }

    public void setBusline(ReportBusline busline) {
        this.busline = busline;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
