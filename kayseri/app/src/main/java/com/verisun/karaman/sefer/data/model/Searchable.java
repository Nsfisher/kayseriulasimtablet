package com.verisun.karaman.sefer.data.model;

public interface Searchable {
    boolean search(String keyword);
}