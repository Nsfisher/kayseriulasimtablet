package com.verisun.karaman.sefer.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 5/31/17.
 */

public class LineCoordinate  {
    /***
     * {
     "latitude":38.715847,
     "longitude":35.492957,
     "direction":"DEPARTURE",
     "busLine":{},
     "city":{},
     "id":"83061"
     }
     */
    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("direction")
    private String direction;

    @SerializedName("id")
    private String id;

    @SerializedName("busLine")
    private NewBusLineDetail busLine;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public NewBusLineDetail getBusLine() {
        return busLine;
    }

    public String getDirection() {
        return direction;
    }

    public String getId() {
        return id;
    }
}
