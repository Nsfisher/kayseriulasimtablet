package com.verisun.karaman.sefer.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nsfisher on 14/06/16.
 */
public class BaseModel implements Serializable {

    @SerializedName("ids")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "id='" + id + '\'' +
                '}';
    }
}
