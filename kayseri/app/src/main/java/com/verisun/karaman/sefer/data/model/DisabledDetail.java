package com.verisun.karaman.sefer.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 15/06/16.
 */
public class DisabledDetail extends  BaseModel {
    /*
    {
        "busLine": "179",
            "start": "776",
            "stop": "779",
            "timeStamp": "/Date(1465898574003+0300)/",
            "userDisableType": "blind",
            "userId": "osman"
    }
    */

      @SuppressWarnings("busLine")
      private String busline;

    @SerializedName("start")
    private String startBusStop;

    @SerializedName("stop")
    private String stopBusStop;

    @SerializedName("userDisableType")
    private String userDisabledType;

    @SerializedName("userId")
    private String userID;

    public String getBusline() {
        return busline;
    }

    public String getStartBusStop() {
        return startBusStop;
    }

    public String getStopBusStop() {
        return stopBusStop;
    }

    public String getUserDisabledType() {
        return userDisabledType;
    }

    public String getUserID() {
        return userID;
    }
    @Override
    public String toString() {
        return "DisabledDetail{" +
                super.toString() +
                "busline='"                 + busline + '\'' +
                "start='"                   +   startBusStop + '\'' +
                "stop='"                    +   stopBusStop + '\'' +
                "userDisableType='"         +   userDisabledType + '\'' +
                "userId='"                  +   userID + '\'' +
                '}';
    }
}
