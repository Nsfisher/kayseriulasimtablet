package com.verisun.karaman.sefer;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.verisun.core.App;
import com.verisun.core.BaseActivity;
import com.verisun.core.Services;
import com.verisun.karaman.sefer.data.model.ReportBody;
import com.verisun.karaman.sefer.data.model.ReportBusline;
import com.verisun.karaman.sefer.utils.PrefUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FinishJourneyConfirmationActivity extends BaseActivity {
    private Services services;
    private String reportType = "OUT_OF_SERVICE";
    private String licensePlate;
    private String busID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_journey_confirmation);
        services = Services.service;

        licensePlate = PrefUtils.getLicensePlate(this);
        busID = (String) getIntent().getExtras().get("busID");

        ButterKnife.findById(this, R.id.button_ok).setEnabled(true);

    }


    public void sendReport(double latitude,double longtitude,String busNo) {
        services.sendReport(App.city_ID, createReportBody(latitude,longtitude,busNo), new Services.BaseCallBack<JsonObject>() {
            @Override
            public void onSuccessResponse(JsonObject jsonObject) {
                Toast.makeText(getApplicationContext(), "Rapor gönderildi.",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String errorMessage) {

            }
        });
    }
    public ReportBody createReportBody(double latitude, double longtitude, String busNo) {
        ReportBody reportBody = new ReportBody();
        reportBody.setLatitude(0);
        reportBody.setLongitude(0);
        reportBody.setBusNo(busNo);
        reportBody.setType(reportType);
        ReportBusline reportBusline = new ReportBusline();
        reportBusline.setId(licensePlate);
        reportBody.setBusline(reportBusline);
        return reportBody;
    }

    @Override
    protected String getAnalyticsScreenName() {
        return "Servis dışı onay";
    }

    @OnClick({R.id.button_cancel, R.id.button_ok})
    public void onClickOk(View view) {
        if (view.getId() == R.id.button_ok) {
            setResult(RESULT_OK);
            if (!TextUtils.isEmpty(busID)) {
                sendReport(0,0,busID);
            }
        }
        finish();
    }
}
