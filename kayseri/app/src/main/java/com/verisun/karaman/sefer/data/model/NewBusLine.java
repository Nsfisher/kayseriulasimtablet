package com.verisun.karaman.sefer.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 14/06/16.
 */
public class NewBusLine extends BaseModel{
    /**
    {
        "type": "BusLine",
            "node": {
                "code": "B1",
                "journeyDuration": 105,
                "name": "BURÇ KAVŞAĞI-IBRAHIMLI-GAZIKENT",
                "type": "1",
                "directions": [
        "DEPARTURE"
        ],
        "id": "231"
    }
    }  **/

    @SerializedName("type")
    private String type;

    @SerializedName("node")
    private  NewBusLineDetail node;



    public String getType() {
        return type;
    }

    public NewBusLineDetail getNode() {
        return node;
    }

    @Override
    public String toString() {
        return "NewBusLineDetail{" +
                super.toString() +
                "type='"                + type + '\'' +
                "node='"                +   node + '\'' +
                '}';
    }

}
