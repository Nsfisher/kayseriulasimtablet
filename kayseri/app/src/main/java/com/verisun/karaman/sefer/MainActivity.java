package com.verisun.karaman.sefer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.drawable.TransitionDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.verisun.core.App;
import com.verisun.core.BaseActivity;
import com.verisun.karaman.sefer.data.model.BusLine;
import com.verisun.karaman.sefer.data.model.NewBusLine;
import com.verisun.karaman.sefer.data.model.Node;
import com.verisun.karaman.sefer.utils.PrefUtils;
import com.verisun.utils.LocationService;


import java.util.List;
import java.util.concurrent.TimeUnit;


import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;


public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
             private boolean settingsDialogShown;
    public static final String EXTRA_LICENSE_PLATE = "EXTRA_LICENSE_PLATE";
    public static final String EXTRA_BUSLINE = "EXTRA_BUSLINE";
    public static final String EXTRA_FIRST_START = "EXTRA_FIRST_START";

    private static final int REQUEST_LICENSE_PLATE = 1001;
    private static final int REQUEST_BUSLINE = 1002;
    private static final int REQUEST_FINISH_JOURNEY = 2001;
             public static final int REQUEST_CHECK_SETTINGS = 1000;

    private static final int MIN_ACTIVITY_CONFIDENCE_INTERVAL = 50;
    private static final int TRANSITION_ANIMATION_TIME = 300;
    private String busID;
    @Bind(R.id.content_frame)
    ViewGroup contentFrame;
    @Bind(R.id.main_license_plate_text)
    TextView licensePlateText;
    @Bind(R.id.main_busline_text)
    TextView busLineText;
    @Bind(R.id.main_alert_select)
    View pleaseSelectAlertView;

    @Bind({ R.id.main_edit_busline,
            R.id.main_edit_license_plate })
    List<View> mainScreenViews;

    @Bind(R.id.main_start_journey)
    Button startButton;
    @Bind(R.id.main_finish_journey)
    Button finishButton;

    @Bind(R.id.txt_direction)
    TextView direction;

    private String licensePlate;
    private BusLine busline;
    private Subscription subscription;
    private int lastDetectedActivityType = DetectedActivity.UNKNOWN;
    private MediaPlayer warningMediaPlayer;
    private boolean outOfService;
    private TransitionDrawable backgroundDrawable;
    private Node newBusLine;
    private TextView busDetail,startBus,stopBus;
    private LinearLayout linearLayout;
    private String start;
    private String stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //LocationService.checkPlayServicesAvailable(getActivity());
        backgroundDrawable = (TransitionDrawable)
                ContextCompat.getDrawable(this, R.drawable.background_main);
        backgroundDrawable.setCrossFadeEnabled(true);
        getWindow().setBackgroundDrawable(backgroundDrawable);
        setContentView(R.layout.activity_main);

        busDetail = (TextView) findViewById(R.id.busdetail);

        licensePlate = PrefUtils.getLicensePlate(this);
        licensePlate = null;
        if (TextUtils.isEmpty(licensePlate)) {
            startEnterLicensePlate(true /* firstStart */);
            return;
        }

        busline = PrefUtils.getBusline(this);
        startBusLineSelectActivityIfNotPresent();

        licensePlateText.setText(licensePlate);

        //App.instance.getLocationUtil().setSettingsListener(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
      //  App.instance.getLocationUtil().registerLocationListener((LocationUtil.Listener) this);
    }

             private void setBusLineText(final BusLine busline) {
      //  busLineText.setText(busline.toTextWithBoldCode());
    }

    @Override
    protected String getAnalyticsScreenName() {
        return "Ana Ekran";
    }

    private void startBusLineSelectActivityIfNotPresent() {
        if (busline == null) {
            startBusLineSelect(true /* firstStart */);
        }
    }

    @OnClick(R.id.main_edit_license_plate)
    public void onClickLicensePlate() {
        startEnterLicensePlate(false /* firstStart */);
    }

    @OnClick(R.id.main_edit_busline)
    public void onClickBusLine() {
        startBusLineSelect(false /* firstStart */);
    }

    private void startEnterLicensePlate(boolean firstStart) {
        final Intent intent = new Intent(this, EnterLicensePlateActivity.class)
                .putExtra(EXTRA_FIRST_START, firstStart);
        startActivityForResult(intent, REQUEST_LICENSE_PLATE);
    }

    private void startBusLineSelect(boolean firstStart) {
        final Intent intent = new Intent(this, BuslineSelectActivity.class)
                .putExtra(EXTRA_FIRST_START, firstStart);
        startActivityForResult(intent, REQUEST_BUSLINE);
    }

    @OnClick(R.id.main_start_journey)
    public void onClickStartJourney() {

        if (outOfService) {
            startButton.setEnabled(false);
            setOutOfService(false);
        } else {
            Intent intent=new Intent(this,JourneyActivity.class);
            intent.putExtra("busID",busID);
            if(!TextUtils.isEmpty(newBusLine.getDirection()))
            intent.putExtra("direction", newBusLine.getDirection());
            startActivity(intent);
        }

    }

    @OnClick(R.id.main_finish_journey)
    public void onClickFinishJourney() {
        startActivityForResult(new Intent(this, FinishJourneyConfirmationActivity.class)
              .putExtra("busID",busID)
                .putExtra("plate",licensePlate),
                REQUEST_FINISH_JOURNEY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      //  LocationService.checkPlayServicesAvailable(getActivity());
        if (resultCode == RESULT_OK) {
            if (REQUEST_LICENSE_PLATE == requestCode) {
                licensePlate = data.getStringExtra(EXTRA_LICENSE_PLATE);
                licensePlateText.setText(licensePlate);

                PrefUtils.setLicensePlate(this, licensePlate);
                startBusLineSelectActivityIfNotPresent();
            } else if (REQUEST_BUSLINE == requestCode) {
                newBusLine = (Node) data.getExtras().get("busline");
                if (newBusLine != null) {
                    busDetail.setText(newBusLine.getBusLineDetail().getCode()+ "  "+newBusLine.getBusLineDetail().getName());
                    direction.setText(newBusLine.getDirectionDescription());
                    busID = newBusLine.getBusLineDetail().getBusID();
                }
                setBusLineText(busline);

                PrefUtils.setBusline(this, newBusLine);
            } else if (REQUEST_FINISH_JOURNEY == requestCode) {
                setOutOfService(true);
            }
        }
    }

    @SuppressLint("InlinedApi")
    private void setOutOfService(final boolean outOfService) {
        this.outOfService = outOfService;

        if (outOfService) {
            backgroundDrawable.startTransition(TRANSITION_ANIMATION_TIME);
        } else {
            backgroundDrawable.reverseTransition(TRANSITION_ANIMATION_TIME);
        }

        ButterKnife.apply(mainScreenViews,
                (view, index) -> view.setVisibility(outOfService ? View.INVISIBLE : View.VISIBLE));
        finishButton.setClickable(!outOfService);

        if (outOfService) {
            startButton.animate()
                    .setInterpolator(new DecelerateInterpolator())
                    .translationX(-startButton.getX())
                    .translationY(contentFrame.getBottom() - startButton.getBottom() + contentFrame.getPaddingBottom());

            finishButton.animate()
                    .setInterpolator(new DecelerateInterpolator())
                    .translationX((contentFrame.getWidth() - finishButton.getWidth()) / 2 - finishButton.getX());
        } else {
            startButton.animate()
                    .setInterpolator(new DecelerateInterpolator())
                    .translationX(0)
                    .translationY(0)
                    .withEndAction(() -> startButton.setEnabled(true));
            finishButton.animate()
                    .setInterpolator(new DecelerateInterpolator())
                    .translationX(0);
        }
        startButton.setCompoundDrawablesWithIntrinsicBounds(0,
                outOfService ? R.drawable.ic_action_start_small : R.drawable.ic_action_start, 0, 0);
        startButton.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
                outOfService ? 14 : 32);
    }

//    @Override
//    public void activityUpdated(DetectedActivity activity) {
//
//        if (!outOfService && lastDetectedActivityType != activity.getType()) {
//            lastDetectedActivityType = activity.getType();
//
//            if (subscription != null) {
//                subscription.unsubscribe();
//            }
//
//            if (warningMediaPlayer != null) {
//                warningMediaPlayer.pause();
//            }
//
//            if (isProbablyInVehicle(activity)) {
//
//                subscription = Observable.interval(0, 2, TimeUnit.SECONDS)
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnUnsubscribe(() ->
//                                runOnUiThread(() ->
//                                        pleaseSelectAlertView.setVisibility(View.INVISIBLE)))
//                        .subscribe(interval -> {
//                            if (interval % 2 == 0) {
//                                pleaseSelectAlertView.setVisibility(View.VISIBLE);
//                            } else {
//                                pleaseSelectAlertView.setVisibility(View.INVISIBLE);
//                            }
//                        });
//
//                if (warningMediaPlayer == null) {
//                    warningMediaPlayer = MediaPlayer.create(this, R.raw.car_lights_warning_tone);
//                    warningMediaPlayer.setLooping(true);
//                }
//                //warningMediaPlayer.start();
//            }
//        }
//    }

    @Override
    protected void onPause() {
        super.onPause();

        if (subscription != null) {
            subscription.unsubscribe();
            lastDetectedActivityType = DetectedActivity.UNKNOWN;
        }

        if (warningMediaPlayer != null) {
            warningMediaPlayer.pause();
        }
    }

             @Override
             protected void onResume() {
                 super.onResume();
               //  LocationService.checkPlayServicesAvailable(getActivity());
             }

             private boolean isProbablyInVehicle(DetectedActivity activity) {
        return activity.getConfidence() > MIN_ACTIVITY_CONFIDENCE_INTERVAL
                && activity.getType() != DetectedActivity.STILL
                && activity.getType() != DetectedActivity.UNKNOWN;
    }

         }
