package com.verisun.karaman.sefer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JourneyCancelConfirmationFragment extends Fragment {

    @Bind(R.id.button_cancel)
    Button buttonCancel;
    @Bind(R.id.button_ok)
    Button buttonOk;

    public JourneyCancelConfirmationFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cancel_journey_confirmation, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        buttonCancel.setText(R.string.no);
        buttonOk.setText(R.string.yes);
        buttonOk.setEnabled(true);
    }

    @OnClick(R.id.button_cancel)
    public void onClickCancel() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    @OnClick(R.id.button_ok)
    public void onClickOk() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }
}
