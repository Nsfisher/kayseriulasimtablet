package com.verisun.karaman.sefer;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.verisun.core.App;
import com.verisun.core.BaseActivity;
import com.verisun.core.Logger;
import com.verisun.core.Services;
import com.verisun.karaman.sefer.data.model.BusLine;
import com.verisun.karaman.sefer.data.model.LineCoordinate;
import com.verisun.karaman.sefer.data.model.ReportBody;
import com.verisun.karaman.sefer.data.model.ReportBusline;
import com.verisun.karaman.sefer.utils.PrefUtils;
import com.verisun.utils.ChangeLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class JourneyActivity extends BaseActivity implements ChangeLocation  {

    private static final String TAG = JourneyActivity.class.getSimpleName();
    public static double latitude;
    public static double longtitude;
    public  boolean statusViolation;


    private final String CANCEL_FRAGMENT = "CANCEL_FRAGMENT";
    @Bind(R.id.journey_license_plate_text)
    TextView licensePlateText;
    @Bind(R.id.journey_busline_text)
    TextView busLineText;
    @Bind(R.id.journey_alert_text)
    TextView journeyAlertView;

    private String reportType = "OUT_OF_LINE";



    private String licensePlate;
    private BusLine busline;
    private LinearLayout linearLayout,linearLayout1,linearLayout2;
    private    ArrayList<LineCoordinate> coordinatesSource=new ArrayList<>();
    private JOURNEY_ROUTE currentRoute = JOURNEY_ROUTE.IDLE;
    private Random random = new Random();
    private Subscription subscribe;
    private TextView startBus,stopBus,startBus1,stopBus1,startBus2,stopBus2;
    private MediaPlayer warningMediaPlayer;
    private String busStartOne;
    private String busStopOne;
    private String busStartTwo;
    private String busStopTwo;
    private String busStartThree;
    private String busStopThree;
    private String busID;
    private String direction;
    private boolean settingsDialogShown;
    private Services services;
    private static final int MESSAGE_REFRESH = 101;
    private static final long REFRESH_TIMEOUT_MILLIS = 30000;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_REFRESH:
                  //  disableRequest(busID);

                    mHandler.sendEmptyMessageDelayed(MESSAGE_REFRESH, REFRESH_TIMEOUT_MILLIS);
                    if (!coordinatesSource.isEmpty()) {
                        calculateDistanceLocation(coordinatesSource,latitude,longtitude);
                        if (!statusViolation) {
                            currentRoute = JOURNEY_ROUTE.WRONG;
                            updateJourneyType(currentRoute);
                            sendReport(latitude, longtitude, coordinatesSource.get(0).getBusLine().getBusID());
                        } else {
                            timeHandler();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    updateJourneyType(JOURNEY_ROUTE.IDLE);
                                }
                            }, 2000);

                        }
                    }
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey);
       // App.instance.getLocationUtil().setSettingsListener(this);

        licensePlate = PrefUtils.getLicensePlate(this);
        busline = PrefUtils.getBusline(this);

        linearLayout1 = (LinearLayout) findViewById(R.id.linear1);
        linearLayout2 = (LinearLayout) findViewById(R.id.linear2);


        startBus2 = (TextView) findViewById(R.id.start_busline2);
        stopBus2 = (TextView) findViewById(R.id.last_stop2);

        busID = (String) getIntent().getExtras().get("busID");
        direction = (String) getIntent().getExtras().get("direction");

        licensePlateText.setText(licensePlate);
        busLineText.setText(busline.toTextWithBoldCode());

        services = Services.service;
        updateJourneyType(currentRoute);
        lineCoordinates();

    }

    @Override
    protected void onResume() {
        super.onResume();
     //   mHandler.sendEmptyMessage(MESSAGE_REFRESH);
      //  timeHandler();
    }


    public void timeHandler() {
        subscribe = Observable.interval(0, 100, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(time -> {

                    Logger.i(TAG, String.format("Requesting route status at time %d.", time));

                    //Show the IDLE state 10 seconds (2 intervals)
                    if (time % 3 != 0) {
                        currentRoute = JOURNEY_ROUTE.IDLE;
                    } else {
                        if (random.nextBoolean()) {
                            currentRoute = JOURNEY_ROUTE.CORRECT;
                        }
                    }

                    updateJourneyType(currentRoute);


                });

    }


    public void lineCoordinates() {
        // TODO: 6/2/17  busID degisecek
        busID = "125243";
        services.lineCoordinates(App.city_ID, busID, direction,new Services.BaseCallBack<ArrayList<LineCoordinate>>() {
            @Override
            public void onSuccessResponse(ArrayList<LineCoordinate> lineCoordinates) {
                if (!lineCoordinates.isEmpty()) {
                    coordinatesSource = lineCoordinates;
                    calculateDistanceLocation(lineCoordinates,latitude,longtitude);
                    if (!statusViolation) {
                        currentRoute = JOURNEY_ROUTE.WRONG;
                        updateJourneyType(currentRoute);
                        sendReport(latitude, longtitude, lineCoordinates.get(0).getBusLine().getBusID());
                    } else {
                        currentRoute=JOURNEY_ROUTE.CORRECT;
                        updateJourneyType(currentRoute);

                    }
                    mHandler.sendEmptyMessage(MESSAGE_REFRESH);
                }

            }
            @Override
            public void onError(String errorMessage) {
                Log.e("","");
            }
        });
    }

    public  void calculateDistanceLocation(ArrayList<LineCoordinate> coordinates,double lati,double longi) {
        double distance;
        for (int i = 0; i <coordinates.size() ; i++) {
            distance = calculateDistance(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude(), lati, longi);
           //  distance = calculateDistance(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude(), 38.724873, 35.485831);
            if (distance < 50) {
                Log.e("MINDISTANCE", "" + distance);
                statusViolation = true;
                break;
            } else {
                statusViolation = false;
            }

        }
    }
    public  double calculateDistance(double firstlat,double firsLong,double secondLati,double seconLongi) {
        float distance;
        Location firstLocation = new Location("first");
        Location secondLocation = new Location("second");

        firstLocation.setLatitude(firstlat);
        firstLocation.setLongitude(firsLong);

        secondLocation.setLatitude(secondLati);
        secondLocation.setLongitude(seconLongi);
        Log.e("DISTANCE:", "" + firstLocation.distanceTo(secondLocation));
        return distance = firstLocation.distanceTo(secondLocation);
    }

    public void sendReport(double latitude,double longtitude,String busNo) {
        services.sendReport(App.city_ID, createReportBody(latitude,longtitude,busNo), new Services.BaseCallBack<JsonObject>() {
            @Override
            public void onSuccessResponse(JsonObject jsonObject) {
                Toast.makeText(getApplicationContext(), "Rapor gönderildi.",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String errorMessage) {

            }
        });
    }
    public ReportBody createReportBody(double latitude,double longtitude,String busNo) {
        ReportBody reportBody = new ReportBody();
        reportBody.setLatitude(latitude);
        reportBody.setLongitude(longtitude);
        reportBody.setBusNo(busNo);
        reportBody.setType(reportType);
        ReportBusline reportBusline = new ReportBusline();
        reportBusline.setId(licensePlate);
        reportBody.setBusline(reportBusline);
        return reportBody;
    }

    @Override
    protected void onPause() {

        super.onPause();
        mHandler.removeMessages(MESSAGE_REFRESH);
        Logger.i(TAG, "Timer unsubscribed");
       // subscribe.unsubscribe();

        if (warningMediaPlayer != null) {
            warningMediaPlayer.pause();
        }
    }

    @SuppressWarnings("deprecation")
    private void updateJourneyType(JOURNEY_ROUTE type) {

        if (type == JOURNEY_ROUTE.WRONG) {
            if (warningMediaPlayer == null) {
                warningMediaPlayer = MediaPlayer.create(this, R.raw.car_lights_warning_tone);
                warningMediaPlayer.setLooping(true);
            }
            warningMediaPlayer.start();
        } else {
            if (warningMediaPlayer != null) {
                warningMediaPlayer.pause();
            }
        }

        final CharSequence text = type.getText(this, busline);
        Logger.i(TAG, "Updating journey route " + text);
        journeyAlertView.setText(text);
        final AnimationDrawable d = (AnimationDrawable) getResources().getDrawable(type.getDrawableTop());
        journeyAlertView.setCompoundDrawablesWithIntrinsicBounds(null, d, null, null);
        if (d != null) {
            d.start();
        }

        getWindow().setBackgroundDrawableResource(type.getColorBackground());
    }

    @OnClick(R.id.journey_cancel)
    public void onClickJourneyCancel() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new JourneyCancelConfirmationFragment(), CANCEL_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        final Fragment cancellationFrag = getSupportFragmentManager().findFragmentByTag(CANCEL_FRAGMENT);
        if (cancellationFrag == null) {
            onClickJourneyCancel();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected String getAnalyticsScreenName() {
        return "Sefer Ekranı";
    }

    @Override
    public void changeLocation(double lati, double longi) {
        Log.e("St", "");
    }

    private enum JOURNEY_ROUTE {
        IDLE(R.string.journey_route_idle, R.drawable.ic_road, R.color.colorPrimary) {
            @Override
            public CharSequence getText(Context context, BusLine busLine) {
                return context.getString(text, busLine.getCode());
            }
        },
        CORRECT(R.string.journey_route_correct, R.drawable.ic_checkmark, R.color.colorPrimaryGreen),
        WRONG(R.string.journey_route_wrong, R.drawable.ic_warning, R.color.colorPrimaryOutOfService);

        final int text;
        final int drawableTop;
        private final int colorBackground;

        JOURNEY_ROUTE(@StringRes int text,
                      @DrawableRes int drawableTop,
                      @DrawableRes int colorBackground) {
            this.text = text;
            this.drawableTop = drawableTop;
            this.colorBackground = colorBackground;
        }

        @DrawableRes
        public int getDrawableTop() {
            return drawableTop;
        }

        @DrawableRes
        public int getColorBackground() {
            return colorBackground;
        }

        public CharSequence getText(Context context, BusLine busLine) {
            return context.getString(text);
        }
    }

    public void disableRequest(String busID) {

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("busLine", busID);






        final JSONObject[] buslineObjects = new JSONObject[1];
        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.POST,
                "http://apiavm.getpoi.com:9060/WcfService/rest/getCheckIns",
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject jsonOb = null;
                        try {
                            JSONArray disabled = (JSONArray) response.getJSONArray("getCheckInsResult");
                            for (int i = 0; i < disabled.length(); i++) {
                                jsonOb = disabled.getJSONObject(i);
                                if (i == 0) {
                                    busStartOne = (String) jsonOb.get("start");
                                    busStopOne = (String) jsonOb.get("stop");
                                } else if (i == 1) {
                                    busStartTwo = (String) jsonOb.get("start");
                                    busStopTwo  = (String) jsonOb.get("stop");
                                } else if (i == 2) {
                                    busStartThree = (String) jsonOb.get("start");
                                    busStopThree = (String) jsonOb.get("stop");
                                }
                            }
                            // ArrayList<Disable> asd = (ArrayList<Disable>) response.get("getCheckInsResult");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (busStartOne != null && busStopOne != null) {
                            startBus.setText(busStartOne + "  ->");
                            stopBus.setText(" "+busStopOne);
                            linearLayout.setVisibility(View.VISIBLE);
                        } else {
                            linearLayout.setVisibility(View.GONE);
                        }
                        if (busStartTwo != null && busStopTwo != null) {
                            startBus1.setText(busStartTwo + "  ->");
                            stopBus1.setText(" "+busStopTwo);
                            linearLayout1.setVisibility(View.VISIBLE);

                        } else {
                            linearLayout1.setVisibility(View.GONE);
                        }
                        if (busStartThree != null && busStopThree != null) {
                            startBus2.setText(busStartThree + "  ->");
                            stopBus2.setText(" "+busStopThree);
                            linearLayout2.setVisibility(View.VISIBLE);

                        } else {
                            linearLayout2.setVisibility(View.GONE);
                        }


                        Logger.setKeyValue("Response", "" + response);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.setKeyValue("Error", "" + error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        App.instance.addToRequestQueue(myRequest, "tag");

    }


}
