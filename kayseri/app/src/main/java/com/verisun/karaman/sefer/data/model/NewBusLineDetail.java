package com.verisun.karaman.sefer.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nsfisher on 14/06/16.
 */
public class NewBusLineDetail extends  BaseModel implements Parcelable{
         @SerializedName("code")
         private String code;

        @SerializedName("journeyDuration")
        private String journeyDuration;

        @SerializedName("name")
        private String name;

        @SerializedName("type")
        private String type;

        @SerializedName("directions")
        private ArrayList<String> directions;

        @SerializedName("id")
        private String busID;

        @SerializedName("direction")
        private String direction;


       @SerializedName("directionDescription")
       private String directionDescription;

    protected NewBusLineDetail(Parcel in) {
        code = in.readString();
        journeyDuration = in.readString();
        name = in.readString();
        type = in.readString();
        directions = in.createStringArrayList();
        busID = in.readString();
        direction = in.readString();
        directionDescription = in.readString();
    }

    public static final Creator<NewBusLineDetail> CREATOR = new Creator<NewBusLineDetail>() {
        @Override
        public NewBusLineDetail createFromParcel(Parcel in) {
            return new NewBusLineDetail(in);
        }

        @Override
        public NewBusLineDetail[] newArray(int size) {
            return new NewBusLineDetail[size];
        }
    };

    public String getDirection() {
                return direction;
        }

        public String getDirectionDescription() {
                return directionDescription;
        }

        public String getBusID() {
                return busID;
        }

        public String getName() {
                return name;
        }

        public String getCode() {
                return code;
        }

        public String getType() {
                return type;
        }

        @Override
        public String getId() {
                return super.getId();
        }

        public ArrayList<String> getDirections() {
                return directions;
        }


        @Override
        public String toString() {
                return "NewBusLineDetail{" +
                        super.toString() +
                        "code='"                + code + '\'' +
                        "journeyDuration='"     +   journeyDuration + '\'' +
                        "name='"                + name + '\'' +
                        "type='"                + type + '\'' +
                        "id='"                  + busID + '\'' +
                        "directions='"          + directions + '\'' +
                        '}';
        }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(code);
        parcel.writeString(journeyDuration);
        parcel.writeString(name);
        parcel.writeString(type);
        parcel.writeStringList(directions);
        parcel.writeString(busID);
        parcel.writeString(direction);
        parcel.writeString(directionDescription);
    }
}
