package com.verisun.karaman.sefer.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 5/31/17.
 */

public class ReportBusline {

    @SerializedName("id")
    private String Id;

    public void setId(String id) {
        Id = id;
    }
}
