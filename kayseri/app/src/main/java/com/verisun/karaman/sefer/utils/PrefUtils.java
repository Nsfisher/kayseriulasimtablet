package com.verisun.karaman.sefer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.verisun.karaman.sefer.data.model.BusLine;
import com.verisun.karaman.sefer.data.model.NewBusLine;
import com.verisun.karaman.sefer.data.model.Node;

public class PrefUtils {
    private static final String LICENSE_PLATE = "license_plate";
    private static final String BUSLINE_CODE = "busline_code";
    private static final String BUSLINE_NAME = "busline_name";


    public static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    public static String getLicensePlate(Context context) {
        return getPrefs(context).getString(LICENSE_PLATE, null);
    }

    public static void setLicensePlate(Context context, String licensePlate) {
        getPrefs(context).edit().putString(LICENSE_PLATE, licensePlate).apply();
    }

    public static BusLine getBusline(Context context) {
        final String code = getPrefs(context).getString(BUSLINE_CODE, null);
        final String name = getPrefs(context).getString(BUSLINE_NAME, null);

        if (!TextUtils.isEmpty(code)) {
            return new BusLine(code, name);
        }
        return null;
    }

    public static void setBusline(Context context, Node busline) {
        getPrefs(context).edit()
                .putString(BUSLINE_CODE, busline.getBusLineDetail().getCode())
                .putString(BUSLINE_NAME, busline.getBusLineDetail().getName())
                .apply();
    }
}
