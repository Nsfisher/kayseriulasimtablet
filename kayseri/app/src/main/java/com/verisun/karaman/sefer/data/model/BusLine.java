package com.verisun.karaman.sefer.data.model;

import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

public class BusLine implements Parcelable {

    private final String code;
    private final String name;

    public BusLine(String code, String name) {
        this.code = code;
        this.name = name;
    }

    protected BusLine(Parcel in) {
        code = in.readString();
        name = in.readString();
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusLine> CREATOR = new Creator<BusLine>() {
        @Override
        public BusLine createFromParcel(Parcel in) {
            return new BusLine(in);
        }

        @Override
        public BusLine[] newArray(int size) {
            return new BusLine[size];
        }
    };

    @Override
    public String toString() {
        return code + " " + name;
    }

    public CharSequence toTextWithBoldCode() {

        SpannableStringBuilder text = new SpannableStringBuilder();
        return appendBoldText(text, getCode())
                .append(" ")
                .append(getName());
    }

    private SpannableStringBuilder appendBoldText(SpannableStringBuilder text, String code) {
        final int start = text.length();
        text.append(code);
        text.setSpan(new StyleSpan(Typeface.BOLD), start, text.length(), 0);

        return text;
    }
}
