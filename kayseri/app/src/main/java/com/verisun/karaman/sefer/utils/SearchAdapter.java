package com.verisun.karaman.sefer.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.verisun.karaman.sefer.data.model.NewBusLine;
import com.verisun.karaman.sefer.data.model.NewBusLineDetail;
import com.verisun.karaman.sefer.data.model.Node;

import java.util.ArrayList;

/**
 * Created by nsfisher on 15/06/16.
 */
public abstract class SearchAdapter<T> extends BaseAdapter {
    private ArrayList<String> list;
    private ArrayList<String> lineNumber;
    private ArrayList<Node> listObjects;
    public int size;

    public void setNewBusLines(ArrayList<String> newBusLines,ArrayList<String> lineNumaber) {
        this.list = newBusLines;
        this.lineNumber = lineNumaber;
        notifyDataSetChanged();
    }

    public void setListObjects(ArrayList<Node> listObjects) {
        this.listObjects = listObjects;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listObjects != null ? listObjects.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return listObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);


}
