package com.verisun.karaman.sefer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.playlog.internal.LogEvent;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.verisun.core.App;
import com.verisun.core.BaseActivity;
import com.verisun.utils.LocationUtil;

import butterknife.Bind;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;

import static com.verisun.karaman.sefer.MainActivity.REQUEST_CHECK_SETTINGS;

public class EnterLicensePlateActivity extends BaseActivity implements
        LocationUtil.Listener, LocationUtil.SettingsListener  {
    private boolean settingsDialogShown;
    @Bind(R.id.license_plate_text)
    EditText licensePlateText;
    @Bind(R.id.button_ok)
    View buttonOk;
    @Bind(R.id.button_cancel)
    View buttonCancel;
    private GoogleApiClient googleApiClient;

    private boolean firstStart;
    private boolean checkStatus = false;
    private MediaPlayer warningMediaPlayer;
    TextView warningTest;
    Button cancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license_plate);
        App.instance.getLocationUtil().setSettingsListener(this);
        setResult(RESULT_CANCELED);
        warningTest=(TextView)findViewById(R.id.licence_alert_select);
        firstStart = getIntent().getBooleanExtra(MainActivity.EXTRA_FIRST_START, false);
        cancel =(Button)findViewById(R.id.licence_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopWarning();
            }
        });
        if (firstStart) {
            buttonCancel.setEnabled(false);
        }

        RxTextView.textChanges(licensePlateText)
                .map(text -> !TextUtils.isEmpty(text))
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(buttonOk::setEnabled);
        DownTimer();
    }

    @Override
    protected void onPause() {
        stopWarning();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (!firstStart) {
            super.onBackPressed();
        }
    }

    public void stopWarning() {
        if (warningMediaPlayer != null) {
            warningMediaPlayer.pause();
            cancel.setVisibility(View.GONE);
            warningTest.setVisibility(View.GONE);
        }
    }
    public void DownTimer(){

        new CountDownTimer(200000, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (!checkStatus) {
                    if (warningMediaPlayer == null) {
                        warningMediaPlayer = MediaPlayer.create(EnterLicensePlateActivity.this, R.raw.car_lights_warning_tone);
                        warningMediaPlayer.setLooping(true);
                    }
                    warningMediaPlayer.start();
                    warningTest.setVisibility(View.VISIBLE);
                    cancel.setVisibility(View.VISIBLE);
                }
            }

        }.start();
    }
    @OnClick(R.id.button_ok)
    public void onClickOk() {
        checkStatus = true;
        stopWarning();
        if (warningMediaPlayer != null) {
            warningMediaPlayer.pause();
            cancel.setVisibility(View.GONE);
            warningTest.setVisibility(View.GONE);
        }
        final Intent data = new Intent()
                .putExtra(MainActivity.EXTRA_LICENSE_PLATE, licensePlateText.getText().toString());
        setResult(RESULT_OK, data);

        finish();
    }

    @OnClick(R.id.button_cancel)
    public void onClickCancel() {
        finish();
    }

    @Override
    protected String getAnalyticsScreenName() {
        return "Plaka Seçim";
    }

   @Override
    public void locationUpdated(@NonNull Location androidLocation) {

    }

    @Override
    public boolean requestLocationUpdatePeriodically() {
        return false;
    }

    @Override
    public void onShowLocationSettings(LocationSettingsResult result, int status) {
         if (!isContextValid()) {
            return;
        }

        switch (status) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied. The client can initialize location
                // requests here.
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied. But could be fixed by showing the user
                // a dialog.
                try {
                    if (!settingsDialogShown) {
                        result.getStatus().startResolutionForResult(getActivity(),
                                REQUEST_CHECK_SETTINGS);
                        settingsDialogShown = true;
                    }
                } catch (IntentSender.SendIntentException ignored) {
                    Log.e("", "");
                }

                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
       // settingsrequest();
    }

    public void settingsrequest()
    {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(buildGoogleApiClient(getActivity()), builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(EnterLicensePlateActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    protected synchronized GoogleApiClient buildGoogleApiClient(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
        return googleApiClient;
    }
}
