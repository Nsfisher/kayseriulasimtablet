package com.verisun.karaman.sefer.data.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nsfisher on 29/08/16.
 */
public class Node implements Parcelable {
    /**
     * "node":{
     * "busLine":{},
     * "direction":"DEPARTURE",
     * "directionDescription":"ERCİYES"
     * }
     */

    @SerializedName("busLine")
    private NewBusLineDetail busLineDetail;

    @SerializedName("direction")
    private String direction;

    @SerializedName("directionDescription")
    private String directionDescription;

    protected Node(Parcel in) {
        busLineDetail = in.readParcelable(NewBusLineDetail.class.getClassLoader());
        direction = in.readString();
        directionDescription = in.readString();
    }

    public static final Creator<Node> CREATOR = new Creator<Node>() {
        @Override
        public Node createFromParcel(Parcel in) {
            return new Node(in);
        }

        @Override
        public Node[] newArray(int size) {
            return new Node[size];
        }
    };

    public String getDirectionDescription() {
        return directionDescription;
    }

    public String getDirection() {
        return direction;
    }

    public NewBusLineDetail getBusLineDetail() {
        return busLineDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(busLineDetail, i);
        parcel.writeString(direction);
        parcel.writeString(directionDescription);
    }


//    public String code;
//    public String description;
//    public String garage;
//    public int journeyDuration;
//    public String name;
//    public String type;
//    public ArrayList<DIRECTION_TYPE> directions;
//
//    public String iconType;
//    public String id;
//    public String directionDescription;
//    public double latitude;
//    public double longitude;
//    public String county;
//
//
//    protected Node(Parcel in) {
//        code = in.readString();
//        description = in.readString();
//        garage = in.readString();
//        journeyDuration = in.readInt();
//        name = in.readString();
//        type = in.readString();
//        directions = in.createTypedArrayList(DIRECTION_TYPE.CREATOR);
//
//        iconType = in.readString();
//        id = in.readString();
//        directionDescription = in.readString();
//        latitude = in.readDouble();
//        longitude = in.readDouble();
//        county = in.readString();
//    }
//
//    public static final Creator<Node> CREATOR = new Creator<Node>() {
//        @Override
//        public Node createFromParcel(Parcel in) {
//            return new Node(in);
//        }
//
//        @Override
//        public Node[] newArray(int size) {
//            return new Node[size];
//        }
//    };
//
//    public String getCounty() {
//        return county;
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getIconType() {
//        return iconType;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public ArrayList<DIRECTION_TYPE> getDirections() {
//        return directions;
//    }
//
//
//
//    public String getDescription() {
//        return description;
//    }
//
//    public Double getLatitude() {
//        return latitude;
//    }
//
//    public Double getLongitude() {
//        return longitude;
//    }
//
//    public int getJourneyDuration() {
//        return journeyDuration;
//    }
//
//    public String getGarage() {
//        return garage;
//    }
//
//    public String getDirectionDescription() {
//        return directionDescription;
//    }
//
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(code);
//        dest.writeString(description);
//        dest.writeString(garage);
//        dest.writeInt(journeyDuration);
//        dest.writeString(name);
//        dest.writeString(type);
//        dest.writeTypedList(directions);
//
//        dest.writeString(iconType);
//        dest.writeString(id);
//        dest.writeString(directionDescription);
//        dest.writeDouble(latitude);
//        dest.writeDouble(longitude);
//        dest.writeString(county);
//    }
}
