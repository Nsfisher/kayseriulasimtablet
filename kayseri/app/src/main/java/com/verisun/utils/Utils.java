package com.verisun.utils;

import android.content.DialogInterface;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.verisun.core.BaseActivity;

public class Utils {
    public static final int REQUEST_CODE_PLAY_SERVICES_CONNECTION_FAILURE_RESOLUTION = 9000;

    public static boolean checkPlayServicesConnected(BaseActivity activity) {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            // Get the error dialog from Google Play services
            GooglePlayServicesUtil.showErrorDialogFragment(
                    resultCode,
                    activity,
                    REQUEST_CODE_PLAY_SERVICES_CONNECTION_FAILURE_RESOLUTION,
                    activity instanceof DialogInterface.OnCancelListener ?
                            (DialogInterface.OnCancelListener) activity : null);
            return false;
        }
    }
}
