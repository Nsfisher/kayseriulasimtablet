package com.verisun.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.verisun.core.App;

public class AnalyticsUtil {

    public enum CATEGORY {
        CONTACT("Iletisim"),
        FAVOURITE("Favori"),
        BEACON("Beacon");

        String name;

        CATEGORY(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public enum ACTION {
        ADD_LINE("Hat ekle"),
        ADD_STOP("Durak ekle"),
        REMOVE_LINE("Hat cikar"),
        REMOVE_STOP("Durak cikar"),

        BEACON_REGION_ENTERED("Beacon alanina girildi"),
        BEACON_REGION_EXIT("Beacon alanindan cikildi"),
        BEACON_PUSH("Bildirim gonderildi"),
        BEACON_ERROR("hata"),

        ;String name;

        ACTION(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static void sendPageView(String screenName) {
        Tracker tracker = App.instance.getTracker();
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public static void sendEvent(@NonNull String category, @NonNull String action) {
        sendEvent(category, action, null);
    }

    public static void sendEvent(@NonNull String category, @NonNull String action, @Nullable String label) {
        sendEvent(category, action, label, 0);
    }

    public static void sendEvent(@NonNull String category, @NonNull String action, @Nullable String label, long value) {
        Tracker tracker = App.instance.getTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .setValue(value)
                .build());
    }


    public static void sendEvent(@NonNull CATEGORY category, @NonNull ACTION action) {
        sendEvent(category, action, null);
    }

    public static void sendEvent(@NonNull CATEGORY category, @NonNull ACTION action, @Nullable String label) {
        sendEvent(category, action, label, 0);
    }

    public static void sendEvent(@NonNull CATEGORY category, @NonNull ACTION action, @Nullable String label, long value) {
        Tracker tracker = App.instance.getTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category.getName())
                .setAction(action.getName())
                .setLabel(label)
                .setValue(value)
                .build());
    }
}
