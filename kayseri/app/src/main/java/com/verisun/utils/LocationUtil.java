package com.verisun.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.verisun.core.App;
import com.verisun.core.Logger;

import java.util.ArrayList;

/**
 * Created by nsfisher on 5/31/17.
 */

public class LocationUtil implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {
    private static final String TAG = LocationUtil.class.getSimpleName();
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private ArrayList<Listener> listeners = new ArrayList<>();
    private SettingsListener settingsListener;

    private boolean isLocationListening;

    public LocationUtil(Context context) {
        buildGoogleApiClient(context);

        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        App.instance.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks());
    }

    protected synchronized void buildGoogleApiClient(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (connectionHint != null) {
            Logger.i(TAG, "Connected hint:" + connectionHint.toString());
        } else {
            Logger.i(TAG, "Connected");
        }

        LocationSettingsRequest.Builder builder =
                new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(this);

        LocationAvailability availability =
                LocationServices.FusedLocationApi.getLocationAvailability(googleApiClient);

        if (settingsListener != null) {
            settingsListener.onShowLocationSettings(null,
                    availability.isLocationAvailable()
                            ? LocationSettingsStatusCodes.SUCCESS
                            : LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE);
        }

        if (!isLocationListening) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Logger.i(TAG, "Connection failed: error code " + result.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        Logger.i(TAG, "location updated :" + location.toString());
        notifyLocationUpdate(location);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Logger.i(TAG, "Connection suspended. cause " + cause);
        googleApiClient.connect();
    }

    public void registerLocationListener(@NonNull Listener listener) {
        listeners.add(listener);
        googleApiClient.registerConnectionCallbacks(this);
        googleApiClient.registerConnectionFailedListener(this);
        googleApiClient.connect();
    }

    public void setSettingsListener(SettingsListener settingsListener) {
        this.settingsListener = settingsListener;
    }

    public void unregisterLocationListener(@NonNull Listener listener) {
        listeners.remove(listener);

        if (listeners.isEmpty()) {
            stopLocationUpdates();
        }
        googleApiClient.unregisterConnectionCallbacks(this);
        googleApiClient.unregisterConnectionFailedListener(this);
        googleApiClient.disconnect();
    }

    private void startLocationUpdates() {
        Location lastLocation = getLastLocation();
        if (lastLocation != null) {
            notifyLocationUpdate(lastLocation, false /* removeListeners */);
        }
        if (googleApiClient.isConnected()) {
            Logger.i(TAG, "starting location update");
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            isLocationListening = true;
        }
    }

    private void stopLocationUpdates() {
        if (googleApiClient.isConnected()) {
            Logger.i(TAG, "location updates stopped");
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            isLocationListening = false;
        }
    }

    private synchronized void notifyLocationUpdate(@NonNull Location location) {
        notifyLocationUpdate(location, true);
    }

    private synchronized void notifyLocationUpdate(@NonNull Location location,
                                                   boolean removeListeners) {
        int listenersSize = listeners.size();
        for (int i = listenersSize - 1; i >= 0; i--) {
            Listener listener = listeners.get(i);
            listener.locationUpdated(location);
            if (!listener.requestLocationUpdatePeriodically() && removeListeners) {
                listeners.remove(listener);
            }
        }

        if (listeners.isEmpty()) {
            stopLocationUpdates();
        }
    }

    @Nullable
    public Location getLastLocation() {
        return LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        if (settingsListener != null) {
            settingsListener.onShowLocationSettings(locationSettingsResult,
                    locationSettingsResult.getStatus().getStatusCode());
        }
    }

    public interface Listener {
        void locationUpdated(@NonNull Location location);

        boolean requestLocationUpdatePeriodically();
    }

    public interface SettingsListener {

        void onShowLocationSettings(LocationSettingsResult result, int status);

    }

    private class ActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            if (activity instanceof LocationUtil.Listener) {
                registerLocationListener((LocationUtil.Listener) activity);
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            if (activity instanceof LocationUtil.Listener) {
                unregisterLocationListener((LocationUtil.Listener) activity);
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

}

