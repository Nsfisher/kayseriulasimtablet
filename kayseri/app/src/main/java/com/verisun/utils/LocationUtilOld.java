package com.verisun.utils;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.verisun.core.App;
import com.verisun.core.Logger;
import com.verisun.karaman.sefer.ActivityRecognitionService;

import java.util.ArrayList;

public class LocationUtilOld implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {
    private static final String TAG = LocationUtilOld.class.getSimpleName();
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long ACTIVITY_RECOGNITION_UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private DetectedActivity lastDetectedActivity;

    private ArrayList<Listener> listeners = new ArrayList<>(2);
    private ArrayList<ActivityRecognitionListener> activityRecognitionListeners = new ArrayList<>(2);
    private SettingsListener settingsListener;
    private boolean isLocationListening;
    private PendingIntent activityRecognitionPendingIntent;

    public LocationUtilOld(Context context) {
        buildGoogleApiClient(context);
        createLocationRequest();

        ((App) context.getApplicationContext()).registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks());

        activityRecognitionPendingIntent = PendingIntent.getService(context, 0,
                new Intent(context, ActivityRecognitionService.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    protected synchronized void buildGoogleApiClient(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (connectionHint != null) {
            Logger.i(TAG, "Connected hint:" + connectionHint.toString());
        } else {
            Logger.i(TAG, "Connected");
        }

        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            notifyLocationUpdate(lastLocation);
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(this);

        if (!activityRecognitionListeners.isEmpty() && !isLocationListening) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Logger.i(TAG, "Connection failed: error code " + result.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        Logger.i(TAG, "location updated :" + location.toString());
        notifyLocationUpdate(location);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Logger.i(TAG, "Connection suspended. cause " + cause);
        googleApiClient.connect();
    }

    public void registerLocationListener(@NonNull Listener listener) {
        listeners.add(listener);
        if (lastLocation != null) {
            listener.locationUpdated(lastLocation);
        }

        if (!isLocationListening) {
            startLocationUpdates();
        }
    }

    public void unregisterLocationListener(@NonNull Listener listener) {
        listeners.remove(listener);

        if (listeners.isEmpty()) {
            stopLocationUpdates();
        }
    }

    public void registerActivityRecognitionListener(@NonNull ActivityRecognitionListener listener) {
        activityRecognitionListeners.add(listener);
        if (lastDetectedActivity != null) {
            listener.activityUpdated(lastDetectedActivity);
        }

        if (!isLocationListening) {
            startLocationUpdates();
        }
    }

    public void unregisterActivityRecognitionListener(@NonNull ActivityRecognitionListener listener) {
        activityRecognitionListeners.remove(listener);

        if (activityRecognitionListeners.isEmpty()) {
            stopLocationUpdates();
        }
    }

    public void setSettingsListener(SettingsListener settingsListener) {
        this.settingsListener = settingsListener;
    }

    private void startLocationUpdates() {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            notifyLocationUpdate(lastLocation);
        }
        if (googleApiClient.isConnected()) {
            Logger.i(TAG, "starting location update");
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(googleApiClient,
                    ACTIVITY_RECOGNITION_UPDATE_INTERVAL_IN_MILLISECONDS,
                    activityRecognitionPendingIntent);
            isLocationListening = true;
        }
    }

    private void stopLocationUpdates() {
        if (googleApiClient.isConnected()) {
            Logger.i(TAG, "location updates stopped");
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(googleApiClient, activityRecognitionPendingIntent);
            isLocationListening = false;
        }
    }

    private void notifyLocationUpdate(@NonNull Location location) {
        int listenersSize = listeners.size();
        for (int i = listenersSize - 1; i >= 0; i--) {
            Listener listener = listeners.get(i);
            listener.locationUpdated(location);
            if (!listener.requestLocationUpdatePeriodically()) {
                listeners.remove(listener);
            }
        }

        if (listeners.isEmpty()) {
            stopLocationUpdates();
        }
    }

    private void notifyActivityRecognitionUpdate(@NonNull DetectedActivity activity) {
        int listenersSize = activityRecognitionListeners.size();
        for (int i = listenersSize - 1; i >= 0; i--) {
            activityRecognitionListeners.get(i)
                    .activityUpdated(activity);
        }

        if (activityRecognitionListeners.isEmpty()) {
            stopLocationUpdates();
        }
    }

    @Nullable
    public Location getLastLocation() {
        if (lastLocation == null) {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        }
        return lastLocation;
    }

    public void setLastDetectedActivity(DetectedActivity lastDetectedActivity) {
        Logger.i(TAG, "Detected Activity: " + lastDetectedActivity);

        this.lastDetectedActivity = lastDetectedActivity;
        notifyActivityRecognitionUpdate(lastDetectedActivity);
    }

    public DetectedActivity getLastDetectedActivity() {
        return lastDetectedActivity;
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        if (settingsListener != null) {
            settingsListener.onShowLocationSettings(locationSettingsResult);
        }
    }

    public interface Listener {
        void locationUpdated(@NonNull Location location);

        boolean requestLocationUpdatePeriodically();
    }

    public interface ActivityRecognitionListener {
        void activityUpdated(DetectedActivity activity);
    }

    public interface SettingsListener {

        void onShowLocationSettings(LocationSettingsResult result);

    }

    private class ActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            if (activity instanceof LocationUtilOld.Listener) {
                registerLocationListener((Listener) activity);
            }
            if (activity instanceof LocationUtilOld.ActivityRecognitionListener) {
                registerActivityRecognitionListener((LocationUtilOld.ActivityRecognitionListener) activity);
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            if (activity instanceof LocationUtilOld.Listener) {
                unregisterLocationListener((LocationUtilOld.Listener) activity);
            }
            if (activity instanceof LocationUtilOld.ActivityRecognitionListener) {
                unregisterActivityRecognitionListener((LocationUtilOld.ActivityRecognitionListener) activity);
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

}
