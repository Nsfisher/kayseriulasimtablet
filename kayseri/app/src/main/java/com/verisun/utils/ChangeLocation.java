package com.verisun.utils;

/**
 * Created by nsfisher on 6/2/17.
 */

public interface ChangeLocation {
    void changeLocation(double lati, double longi);
}
