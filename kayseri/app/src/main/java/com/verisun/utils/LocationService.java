package com.verisun.utils;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.google.android.gms.gcm.TaskParams;
import com.verisun.core.App;
import com.verisun.karaman.sefer.JourneyActivity;

/**
 * Created by nsfisher on 5/31/17.
 */

public class LocationService extends GcmTaskService {
    private static final String TAG = LocationService.class.getSimpleName();
    private LocationManager locationManager;
    private Location mCurrentLocation;
    private boolean isStatus = false;
    private double firstLat;
    private double firstLong;



    public static final String TASK_GET_LOCATION_ONCE = "location_oneoff_task";
    public static final String TASK_GET_LOCATION_PERIODIC = "location_periodic_task";


    private static final int RC_PLAY_SERVICES = 123;

    @Override
    public void onInitializeTasks() {
        startPeriodicLocationTask(TASK_GET_LOCATION_PERIODIC,
                1L, null);
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        Log.d(TAG, "onRunTask: " + taskParams.getTag());

        String tag = taskParams.getTag();
        Bundle extras = taskParams.getExtras();
        int result = GcmNetworkManager.RESULT_SUCCESS;

        switch (tag) {
            case TASK_GET_LOCATION_ONCE:
                //
                startOneOffLocationTask(tag, extras);
                 break;

            case TASK_GET_LOCATION_PERIODIC:
                getLastKnownLocation();
                break;
        }
        return result;
    }


    public void getLastKnownLocation() {
        Location lastKnownGPSLocation;
        Location lastKnownNetworkLocation;
        String gpsLocationProvider = LocationManager.GPS_PROVIDER;
        String networkLocationProvider = LocationManager.NETWORK_PROVIDER;

        try {
            locationManager = (LocationManager) App.instance.getSystemService(Context.LOCATION_SERVICE);

            lastKnownNetworkLocation = locationManager.getLastKnownLocation(networkLocationProvider);
            lastKnownGPSLocation = locationManager.getLastKnownLocation(gpsLocationProvider);

            if (lastKnownGPSLocation != null) {
                Log.i(TAG, "lastKnownGPSLocation is used.");
                this.mCurrentLocation = lastKnownGPSLocation;
                if (mCurrentLocation.getLongitude() != 0) {
                    JourneyActivity.longtitude = mCurrentLocation.getLongitude();
                    JourneyActivity.latitude = mCurrentLocation.getLatitude();
                }
            } else if (lastKnownNetworkLocation != null) {
                Log.i(TAG, "" + lastKnownNetworkLocation.getLatitude());
                this.mCurrentLocation = lastKnownNetworkLocation;
                if (mCurrentLocation.getLongitude() != 0) {
                    JourneyActivity.longtitude = mCurrentLocation.getLongitude();
                    JourneyActivity.latitude = mCurrentLocation.getLatitude();
                }
            } else {
                Log.e(TAG, "lastLocation is not known.");
                return;
            }

        } catch (SecurityException sex) {
            Log.e(TAG, "Location permission is not granted!");
        }

        return;
    }

    public static void startOneOffLocationTask(String tag, Bundle extras) {
        Log.d(TAG, "startOneOffLocationTask");

        GcmNetworkManager mGcmNetworkManager = GcmNetworkManager.getInstance(App.instance);
        OneoffTask.Builder taskBuilder = new OneoffTask.Builder()
                .setService(LocationService.class)
                .setTag(tag);

        if (extras != null) taskBuilder.setExtras(extras);

        OneoffTask task = taskBuilder.build();
        mGcmNetworkManager.schedule(task);
    }

    public static void startPeriodicLocationTask(String tag, Long period, Bundle extras) {
        Log.d(TAG, "startPeriodicLocationTask");

        GcmNetworkManager mGcmNetworkManager = GcmNetworkManager.getInstance(App.instance);
        PeriodicTask.Builder taskBuilder = new PeriodicTask.Builder()
                .setService(LocationService.class)
                .setTag(tag)
                .setPeriod(period)
                .setPersisted(true)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED);

        if (extras != null) taskBuilder.setExtras(extras);

        PeriodicTask task = taskBuilder.build();
        mGcmNetworkManager.schedule(task);
    }


    public static boolean checkPlayServicesAvailable(Activity activity) {
        GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
        int resultCode = availability.isGooglePlayServicesAvailable(App.instance);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (availability.isUserResolvableError(resultCode)) {
                // Show dialog to resolve the error.
                availability.getErrorDialog(activity, resultCode, RC_PLAY_SERVICES).show();
            }
            return false;
        } else {
            return true;
        }
    }


}
