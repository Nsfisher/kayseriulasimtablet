package com.verisun.core;

import android.content.Context;
import android.text.format.DateUtils;

import com.orhanobut.wasp.utils.SSLUtils;
import com.orhanobut.wasp.utils.WaspHttpStack;
import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.OkHttpClient;
import com.verisun.karaman.sefer.BuildConfig;


import java.net.CookieHandler;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

public class MobiettOkHttpStack implements WaspHttpStack<OkHttpStack> {
    private final OkHttpStack okHttpStack;

    public MobiettOkHttpStack(Context context, int keyStoreRawResId, String keyStorePassword) {
        OkHttpClient okHttpClient = App.instance.getOkHttpClient().clone();
        okHttpClient.setConnectionPool(new ConnectionPool(2, DateUtils.MINUTE_IN_MILLIS));

        if (BuildConfig.DEBUG) {
            okHttpClient.setSslSocketFactory(SSLUtils.getTrustAllCertSslSocketFactory());
            okHttpClient.setHostnameVerifier(SSLUtils.getEmptyHostnameVerifier());
        } else {
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    HostnameVerifier hv =
                            HttpsURLConnection.getDefaultHostnameVerifier();
                    return hostname.equals(BuildConfig.HOST_NAME) && hv.verify("verisun.com", session);
                }
            });
            okHttpClient.setSslSocketFactory(SSLUtils.getPinnedCertSslSocketFactory(context, keyStoreRawResId, keyStorePassword));
        }
        this.okHttpStack = new OkHttpStack(okHttpClient);
    }

    public OkHttpStack getHttpStack() {
        return this.okHttpStack;
    }

    public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
    }

    public void setSslSocketFactory(SSLSocketFactory sslSocketFactory) {
    }

    public void setCookieHandler(CookieHandler cookieHandler) {
    }
}