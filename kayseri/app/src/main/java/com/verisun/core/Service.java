package com.verisun.core;

import android.util.Base64;

import com.orhanobut.wasp.Callback;
import com.orhanobut.wasp.Response;
import com.orhanobut.wasp.Wasp;
import com.orhanobut.wasp.WaspError;
import com.orhanobut.wasp.WaspRequest;
import com.orhanobut.wasp.http.Body;
import com.orhanobut.wasp.http.GET;
import com.orhanobut.wasp.http.POST;
import com.orhanobut.wasp.http.Path;
import com.orhanobut.wasp.parsers.GsonParser;
import com.orhanobut.wasp.utils.AuthToken;
import com.orhanobut.wasp.utils.LogLevel;
import com.orhanobut.wasp.utils.RequestInterceptor;
import com.orhanobut.wasp.utils.SimpleRequestInterceptor;
import com.orhanobut.wasp.utils.WaspRetryPolicy;
import com.verisun.karaman.sefer.BuildConfig;
import com.verisun.karaman.sefer.R;
import com.verisun.karaman.sefer.data.model.Disable;
import com.verisun.karaman.sefer.data.model.NewBusLine;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by nsfisher on 14/06/16.
 */
public interface Service {
    String BaseURzL = "https://159.8.34.171:8443/ekent/rest";
    String username = "android";
    String password = "EQcgGLecuVMuiV0B46gO";
    String BaseURL = String.format("https://%s:%s/smartcity/rest", BuildConfig.HOST_NAME, BuildConfig.HOST_PORT);

    Service service = new Wasp.Builder(App.instance)
            .setEndpoint(Service.BaseURL)
            .setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void onHeadersAdded(Map<String, String> stringStringMap) {
                }

                @Override
                public void onQueryParamsAdded(Map<String, Object> stringObjectMap) {

                }

                @Override
                public WaspRetryPolicy getRetryPolicy() {
                    return null;
                }

                @Override
                public AuthToken getAuthToken() {
                    return new AuthToken("Basic " + Base64.encodeToString((Service.username + ":" + Service.password).getBytes(), Base64.NO_WRAP));
                }
            })
            .trustCertificates()
            .setParser(new GsonParser(App.instance.getGson()))
            .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
            .build()
            .create(Service.class);

    @GET("/search/{keywords}")
    WaspRequest search(@Path("keywords") String keyword, BaseCallBack<ArrayList<NewBusLine>> callBack);

    @POST("/getCheckIns")
    WaspRequest checkDisabled(@Body Object busline, BaseCallBack<ArrayList<Disable>> callBack);

    String baseDisable = " http://apiavm.getpoi.com:9060/WcfService/rest";

    Service disabledService= new Wasp.Builder(App.instance)
            .setEndpoint(Service.baseDisable)
            .setRequestInterceptor(new SimpleRequestInterceptor() {
                @Override
                public void onHeadersAdded(Map<String, String> headers) {
                    super.onHeadersAdded(headers);
                    headers.put("Content-Type", "application/json");
                }

                @Override
                public void onQueryParamsAdded(Map<String, Object> params) {

                }

                @Override
                public WaspRetryPolicy getRetryPolicy() {
                    return null;
                }

                @Override
                public AuthToken getAuthToken() {
                    return null;
                }
            }).trustCertificates()
            .setParser(new GsonParser(App.instance.getGson()))
            .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
            .build()
            .create(Service.class);



    abstract class BaseCallBack<T> implements Callback<T> {

        abstract public void onSuccess(T t);
        abstract public void onError(String errorMessage);

        @Override
        public void onSuccess(Response response, T t) {
            Logger.setKeyValue("RESPONSE_URL=", response.getUrl());
            Logger.setKeyValue("RESPONSE_BODY=", response.getBody());
            onSuccess(t);
        }

        @Override
        public void onError(WaspError waspError) {
            Logger.setKeyValue("RESPONSE_URL", waspError.getResponse().getUrl());
            Logger.setKeyValue("RESPONSE_BODY", waspError.getResponse().getBody());
            Logger.setKeyValue("RESPONSE_ERROR", waspError.getErrorMessage());
            Logger.setKeyValue("RESPONSE_STATUS_CODE", String.valueOf(waspError.getResponse().getStatusCode()));
            switch (waspError.getResponse().getStatusCode()) {
                case -1:
//                    onError(App.instance.getString(R.string.err_no_network));
//                    break;
                default:
                    onError(App.instance.getString(R.string.err_general));
            }
        }
    }


}
