package com.verisun.core;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.verisun.utils.AnalyticsUtil;
import com.verisun.utils.Utils;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity
        implements DialogInterface.OnCancelListener {

    protected final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        checkPlayServices();
        AnalyticsUtil.sendPageView(getAnalyticsScreenName());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    protected boolean isContextValid() {
        boolean isContextValid = !isFinishing()
                && (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1 || !isDestroyed());
        if (!isContextValid) {
            Logger.log(this.getClass().getName() + " context is not valid");
        }
        return isContextValid;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            int visibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                visibility = visibility
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }
            getWindow().getDecorView().setSystemUiVisibility(visibility);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isContextValid() && item.getItemId() == android.R.id.home) {
            try {
                onBackPressed();
            } catch (Exception ignored) { }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected abstract String getAnalyticsScreenName();

    final protected void checkPlayServices() {
        if (Utils.checkPlayServicesConnected(this)) {
            playServicesReady();
        }
    }

    protected void playServicesReady() {
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Utils.REQUEST_CODE_PLAY_SERVICES_CONNECTION_FAILURE_RESOLUTION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        checkPlayServices();
                        break;
                    case Activity.RESULT_CANCELED:
                        if (isContextValid()) {
                            new AlertDialog.Builder(this)
                                    .setMessage("Google Play Services must be installed.")
                                    .setCancelable(false)
                                    .setNeutralButton("OK", null)
                                    .setOnCancelListener(this)
                                    .show();
                        }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public BaseActivity getActivity() {
        return this;
    }

    public App getApp() {
        return (App) getApplication();
    }
}
