package com.verisun.core;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.JsonObject;
import com.orhanobut.wasp.Callback;
import com.orhanobut.wasp.Response;
import com.orhanobut.wasp.Wasp;
import com.orhanobut.wasp.WaspError;
import com.orhanobut.wasp.WaspRequest;
import com.orhanobut.wasp.http.Body;
import com.orhanobut.wasp.http.GET;
import com.orhanobut.wasp.http.POST;
import com.orhanobut.wasp.http.Path;
import com.orhanobut.wasp.http.Query;
import com.orhanobut.wasp.parsers.GsonParser;
import com.orhanobut.wasp.utils.AuthToken;
import com.orhanobut.wasp.utils.LogLevel;
import com.orhanobut.wasp.utils.SimpleRequestInterceptor;
import com.orhanobut.wasp.utils.WaspRetryPolicy;
import com.verisun.karaman.sefer.BuildConfig;
import com.verisun.karaman.sefer.R;
import com.verisun.karaman.sefer.data.model.LineCoordinate;
import com.verisun.karaman.sefer.data.model.NewBusLine;
import com.verisun.karaman.sefer.data.model.ReportBody;
import com.verisun.karaman.sefer.data.model.Search;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by nsfisher on 5/29/17.
 */

public interface Services {
    String BaseURL = String.format("https://%s:%s/smartcity/rest", BuildConfig.HOST_NAME, BuildConfig.HOST_PORT);
    Services service = new Wasp.Builder(App.instance)
            .setEndpoint(Services.BaseURL)
            .setRequestInterceptor(new SimpleRequestInterceptor() {
                @Override
                public AuthToken getAuthToken() {
                    return new AuthToken(BuildConfig.AUTH_TOKEN);
                }

                @Override
                public void onHeadersAdded(Map<String, String> headers) {
                    super.onHeadersAdded(headers);
                    headers.put("Accept-Encoding", "gzip");
                    headers.put("Content-Type", "application/json");
                  //  headers.put("Accept-Language", LocaleUtils.getLocale(App.instance).getLanguage());


                }

                @Override
                public WaspRetryPolicy getRetryPolicy() {
                    return new WaspRetryPolicy(30000, 1, 1);
                }
            })
            .setWaspHttpStack(new MobiettOkHttpStack(App.instance,
                    R.raw.mobiett_prod_cert, BuildConfig.PROD_KEY_STORE_PASS))
            .setParser(new GsonParser(App.instance.getGson()))
            .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
            .build()
            .create(Services.class);

//https://api-kayseri.verisun.com:8543/smartcity/rest/search/buslines/directions/p?cityId=6
    @GET("/search/buslines/directions" +
            "/{keywords}")
    WaspRequest search(@Query("cityId") String cityId,
                       @Path("keywords") String keyword,
                       BaseCallBack<ArrayList<Search>> callBack);

    //https://api-kayseri.verisun.com:8543/smartcity/rest/administration/lineCoordinates/125293?cityId=6
    //https://api-kayseri.verisun.com:8543/smartcity/rest/administration/lineCoordinates/125243?cityID=6
    @GET("/administration/lineCoordinates/{lineId}&direction={DIRECTION}")
    WaspRequest lineCoordinates(@Query("cityId") String cityId, @Path("lineId") String lineCoordinates,@Path("DIRECTION") String direction, BaseCallBack<ArrayList<LineCoordinate>> callBack);

    @POST("/administration/sendLineReport?cityId=6")
    WaspRequest sendReport(@Query("cityId") String cityId, @Body ReportBody body, BaseCallBack<JsonObject> object);

    abstract class BaseCallBack<T> implements Callback<T> {

        @Override
        public void onSuccess(Response response, T t) {
            Logger.log("RESPONSE = " + t);
            onSuccessResponse(t);
        }

        abstract public void onSuccessResponse(T t);

        abstract public void onError(String errorMessage);

        @Override
        public void onError(WaspError waspError) {
            Logger.log(waspError.toString());

        }
    }

    class SimpleBaseCallBack<T> extends BaseCallBack<T> {

        @Override
        public void onSuccessResponse(T o) {
        }

        @Override
        public void onError(String errorMessage) {
        }
    }
}
