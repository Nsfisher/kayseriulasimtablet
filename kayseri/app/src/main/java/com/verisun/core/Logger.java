package com.verisun.core;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.verisun.karaman.sefer.BuildConfig;

import java.util.HashMap;
import java.util.Set;

import io.fabric.sdk.android.Fabric;


public class Logger {

    private static final String TAG = "Logger";

    public static void init(Context context) {
        if (!BuildConfig.DEBUG) {
            Fabric.with(context, new Crashlytics());
        }

        ((App) context.getApplicationContext()).registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks());
    }

    public static void log(String key, String value) {
        log(key + " = " + value);
    }

    public static void setKeyValue(String key, String value) {
        if (Fabric.isInitialized()) {
            Crashlytics.setString(key, value);
        }
        Log.d(TAG, key + " = " + value);
    }

    private static String getBundleMap(Bundle bundle) {
        HashMap<String, Object> map = new HashMap<>();
        if (bundle != null) {
            Set<String> keySet = bundle.keySet();
            for (String key : keySet) {
                if (!key.startsWith("android")) {
                    Object value = bundle.get(key);
                    if (value instanceof Bundle) {
                        map.put(key, getBundleMap((Bundle) value));
                    } else if (value instanceof Class) {
                        map.put(key, ((Class) value).getSimpleName());
                    } else {
                        map.put(key, value);
                    }
                }
            }
        }
        return map.toString();
    }

    public static void log(String key, Bundle bundle) {
        log(key + " = " + getBundleMap(bundle));
    }

    public static void log(String message) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message);
        }
        Log.d(TAG, message);
    }

    public static void i(String tag, String message) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message);
        }
        Log.i(TAG + "/" + tag, message);

    }

    public static void w(String tag, String message) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message);
        }
        Log.w(TAG + "/" + tag, message);

    }

    public static void e(String tag, String message, Throwable e) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(message);
        }
        Log.e(TAG + "/" + tag, message, e);
    }

    public static String getActivityName(Activity activity) {
        String activityName;
        if (activity instanceof BaseActivity) {
            activityName = ((BaseActivity) activity).getAnalyticsScreenName();
        } else {
            activityName = activity.getClass().getSimpleName();
        }
        return activityName;
    }

    private static class ActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            String activityName = getActivityName(activity);
            setKeyValue("activityCreated", activityName);
            log(activityName + " extras", activity.getIntent().getExtras());
            log(activityName + " savedIntanceState", savedInstanceState);
        }

        @Override
        public void onActivityStarted(Activity activity) {
            setKeyValue("activityStarted", getActivityName(activity));
        }

        @Override
        public void onActivityResumed(Activity activity) {
            setKeyValue("activityResumed", getActivityName(activity));
        }

        @Override
        public void onActivityPaused(Activity activity) {
            String activityName = getActivityName(activity);
            setKeyValue("activityPaused", activityName);
            log(activityName + " paused");
        }

        @Override
        public void onActivityStopped(Activity activity) {
            setKeyValue("activityStopped", getActivityName(activity));
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            setKeyValue("activitySaveInstanceState", getActivityName(activity));
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            String activityName = getActivityName(activity);
            setKeyValue("activityDestroyed", activityName);
            log(activityName + " destroyed");
        }
    }
}
