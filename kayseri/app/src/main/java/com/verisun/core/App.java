package com.verisun.core;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.verisun.karaman.sefer.BuildConfig;
import com.verisun.karaman.sefer.R;
import com.verisun.utils.LocationService;
import com.verisun.utils.LocationUtil;
import com.verisun.utils.LocationUtilOld;

import java.io.File;

public class  App extends Application {

    // Cache size for the OkHttpClient
    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    public static String city_ID = "6";
    public static App instance;
    private OkHttpClient okHttpClient;
    private Gson gson;
    private Tracker tracker;
    private RequestQueue mRequestQueue;
    private LocationUtilOld locationUtilOld;
    private LocationUtil locationUtil;
    public static final String TAG = App.class
            .getSimpleName();


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        locationUtilOld = new LocationUtilOld(this);

        gson = new GsonBuilder()
                .setDateFormat("dd.MM.yy HH:mm:ss")
                .create();

        okHttpClient = new OkHttpClient();
        File cacheDir = new File(getCacheDir(), "http");
        //noinspection ResultOfMethodCallIgnored
        cacheDir.mkdirs();
        com.squareup.okhttp.Cache cache = new com.squareup.okhttp.Cache(cacheDir, DISK_CACHE_SIZE);
        okHttpClient.setCache(cache);

        Logger.init(this);

        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        am.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                0);
    }

    public Gson getGson() {
        return gson;
    }

    public Tracker getTracker() {
        if (tracker == null) {
            GoogleAnalytics ga = GoogleAnalytics.getInstance(instance);
            ga.setDryRun(BuildConfig.DEBUG);
            tracker = ga.newTracker(R.xml.analytics);
        }
        return tracker;
    }

    public LocationUtilOld getLocationUtilOld() {
        return locationUtilOld;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }
    public LocationUtil getLocationUtil() {
        if (locationUtil == null) {
            locationUtil = new LocationUtil(this);
        }

        return locationUtil;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10,
                       // HERE IS HOW THE COMPLETE CODE WILL LOOK LIKE
                               -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
